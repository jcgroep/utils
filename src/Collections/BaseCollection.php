<?php namespace Jcgroep\Utils\Collections;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Jcgroep\Utils\ValueObjects\StringObject;
use Lang;

class BaseCollection extends Collection
{
    public function delete()
    {
        $this->each(function (Model $instance) {
            $instance->delete();
        });
    }

    public function isNotEmpty()
    {
        return !$this->isEmpty();
    }

    /**
     * @param callable $callback
     * @return BaseCollection
     */
    public function map(callable $callback)
    {
        if (is_string($callback)) {
            return new static(array_map($callback, $this->items));
        }
        return new static(parent::map($callback));
    }

    public function join($glue = ',', $finalGlue = ''): StringObject
    {
        return StringObject::make(join($glue, $this->items));
    }

    public function fill(int $number, callable $callback)
    {
        for ($i = 0; $i < $number; $i++) {
            $this->add($callback($i));
        }
        return $this;
    }

    public function __toString()
    {
        return '[' . $this->join(',') . ']';
    }

    public function walk(callable $callback)
    {
        $newItems = [];
        $oldItems = $this->items;
        foreach ($oldItems as $key => $item) {
            $callback($item, $key);
            $newItems[$key] = $item;
        }
        return new static($newItems);
    }

    /**
     * Cast primitives to value objects
     * @param $objectClass
     * @return BaseCollection|static
     */
    public function castAll($objectClass)
    {
        return $this->map(function ($value) use ($objectClass) {
            return new $objectClass($value);
        });
    }

    public function filterByInstanceFunction($functionName, $params = [])
    {
        return $this->filter(function ($instance) use ($functionName, $params) {
            return call_user_func([$instance, $functionName], $params);
        });
    }

    public function humanReadable()
    {
        $firstGroup = $this->values()->filter(function ($instance, $index) {
            return $index != $this->count() - 1;
        });
        if ($firstGroup->isEmpty()) {
            return $this->get($this->count() - 1);
        } else {
            return $firstGroup->join(', ') . ' ' . trans('global.and') . ' ' . $this->get($this->count() - 1);
        }
    }

    public function intersectKey($items)
    {
        return new static(array_intersect_key($this->items, $this->getArrayableItems($items)));
    }

    public function unshift($key, $value)
    {
        return new static([$key => $value] + $this->items);
    }

    public function keyBy($column)
    {
        return new static(array_combine($this->pluck($column)->toArray(), $this->items));
    }

    function after($currentItem, $fallback = null) {
        $currentKey = $this->search(function ($item, $key) use ($currentItem) {
            return $item->id == $currentItem;
        }, true);
        if ($currentKey === false) {
            return $fallback;
        }
        $currentOffset = $this->keys()->search($currentKey, true);
        $next = $this->slice($currentOffset, 2);
        if ($next->count() < 2) {
            return $fallback;
        }
        return $next->last();
    }

    public function getPage($page = 1, $perPage = 10)
    {
        $offset = $perPage * $page - $page;
        $this->slice($offset, $offset + $perPage);
    }

    public function mergeAll(Collection $add)
    {
        $collection = new static($this);
        foreach($add as $item){
            $collection->push($item);
        }
        return $collection;
    }
    public function toExportJson() {
        return json_encode($this->map->toExport());
    }
}
