<?php namespace Jcgroep\Utils\ValueObjects\Files;

use App\Models\Utils\ValueObjects\Files\MimeTypeSupport;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Str;

class File
{
    /**
     * @var Filesystem
     */
    protected $filesystem;
    protected static $storageDir;
    protected static $publicPath;
    protected $value;
    protected $directory = '/';

    public static function makeFromUploadedFile(UploadedFile $file)
    {
        $file->move(storage_path(), $file->getClientOriginalName());
        return self::make(storage_path() . '/' . $file->getClientOriginalName());
    }

    public static function makeFromBase64($base64, $fileName)
    {
        $newFileName = storage_path() . '/' . $fileName;
        (new Filesystem)->put($newFileName, base64_decode($base64));
        return self::make($newFileName);
    }

    public static function make($value)
    {
        $instance = new static($value);
        if ($instance->exists()) {
            if ($instance->isVideo()) {
                return new VideoFile($value);
            } elseif ($instance->isImage()) {
                return new ImageFile($value);
            } elseif ($instance->isAudio()) {
                return new AudioFile($value);
            } elseif (in_array($instance->mimeType(), MimeTypeSupport::officeFiles())) {
                return new OfficeFile($value);
            } elseif (in_array($instance->mimeType(), MimeTypeSupport::pdf())) {
                return new PdfFile($value);
            }
            return new TextFile($value);
        }
        return $instance;
    }

    public function __construct($value)
    {
        $this->filesystem = new Filesystem;
        self::$storageDir = storage_path('/app/public/');
        self::$publicPath = '/storage/';
        $this->value = $this->convertPathToAbsolute($value);
    }

    protected function convertPathToAbsolute($value)
    {
        if (Str::startsWith($value, '/') && !Str::startsWith($value, self::$publicPath)) {
            return $value;
        }
        $this->directory = $this->filesystem->dirname($value) . '/';

        return self::$storageDir . $value;
    }

    public function getSubdir()
    {
        return 'files/';
    }

    public function getRelativePath()
    {
        if ($this->directory == '/') {
            return str_replace(public_path('storage'), '', str_replace(storage_path('app/public'), '', str_replace('//', '/', $this->getAbsolutePath())));
        }
        return $this->directory . $this->name();
    }

    public function getPublicPath()
    {
        return self::$publicPath . $this->getRelativePath();
    }

    public function getAbsolutePath()
    {
        return $this->value;
    }

    public function getStoragePath()
    {
        return self::$storageDir . $this->getRelativePath();
    }

    public function isVideo()
    {
        return Str::startsWith($this->mimeType(), 'video/');
    }

    public function isImage()
    {
        return Str::startsWith($this->mimeType(), 'image/');
    }

    public function isAudio()
    {
        return Str::startsWith($this->mimeType(), 'audio/') || $this->extension() == 'mp3';
    }

    public function exists()
    {
        return $this->filesystem->exists($this->getAbsolutePath());
    }

    public function delete()
    {
        return $this->filesystem->delete($this->getAbsolutePath());
    }

    public function createUnexistingDirectories()
    {
        if (!$this->filesystem->isDirectory($this->dirname())) {
            $this->filesystem->makeDirectory($this->dirname(), 0755, true);
        }
    }

    public function move($target)
    {
        $target = new static($target);
        $target->createUnexistingDirectories();
        $this->filesystem->move($this->getAbsolutePath(), $target->getStoragePath());
        return $target;
    }

    public function copy($target)
    {
        $target = new static($target);
        $target->createUnexistingDirectories();

        return $this->filesystem->copy($this->getAbsolutePath(), $target->getStoragePath());
    }

    public function name()
    {
        return $this->filesystem->name($this->value) . '.' . $this->extension();
    }

    public function basename()
    {
        return $this->filesystem->basename($this->value);
    }

    public function dirname()
    {
        return $this->filesystem->dirname($this->getAbsolutePath());
    }

    public function extension()
    {
        return strtolower($this->filesystem->extension($this->value));
    }

    public function mimeType()
    {
        return $this->filesystem->mimeType($this->getAbsolutePath());
    }

    public function size()
    {
        return $this->filesystem->size($this->getAbsolutePath());
    }

    public function getHumanReadableSize()
    {
        if (($this->size() >= 1 << 30)) {
            return number_format($this->size() / (1 << 30), 0) . " GB";
        }
        if (($this->size() >= 1 << 20)) {
            return number_format($this->size()/ (1 << 20), 0) . " MB";
        }
        if (($this->size() >= 1 << 10)) {
            return number_format($this->size() / (1 << 10), 0) . " KB";
        }
        return number_format($this->size()) . " B";
    }

    public function isDirectory()
    {
        return $this->filesystem->isDirectory($this->value);
    }

    public function getType()
    {
        return $this->mimeType();
    }

    public function get($lock = false)
    {
        return $this->filesystem->get($this->value, $lock);
    }

    public function put($contents, $lock = false)
    {
        return $this->filesystem->put($this->value, $contents, $lock);
    }

    public function getDirectory()
    {
        if($this->isDirectory()){
            return $this;
        }
        return dirname($this->getAbsolutePath());
    }

    public function getThumbnail()
    {
        return '';
    }

    public function toBase64()
    {
        return base64_encode($this->filesystem->get($this->getAbsolutePath()));
    }

    public function asHtml()
    {
        return '<a href="' . $this->getPublicPath() . '">download</a>';
    }

    public function __toString()
    {
        return $this->getPublicPath();
    }
}
