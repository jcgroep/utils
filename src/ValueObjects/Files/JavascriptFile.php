<?php namespace Jcgroep\Utils\ValueObjects\Files;

use Illuminate\Filesystem\Filesystem;

class JavascriptFile extends File
{
    public static function make($value)
    {
        return new static($value);
    }

    public function __construct($value){
        $this->filesystem = new Filesystem;
        $this->value = resource_path($value);
    }
}