<?php namespace Jcgroep\Utils\ValueObjects\Files;

class TextFile extends File
{

    public function getSubdir()
    {
        return 'text/';
    }

    public function prepend($data)
    {
        return $this->filesystem->prepend($this->value, $data);
    }

    public function append($data)
    {
        return $this->filesystem->append($this->value, $data);
    }
}
