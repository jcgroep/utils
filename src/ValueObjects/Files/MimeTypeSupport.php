<?php namespace Jcgroep\Utils\ValueObjects\Files;

class MimeTypeSupport
{
    public static function audio()
    {
        return [
            'audio/mpeg3',
            'audio/x-mpeg-3',
            'audio/mpeg',
            'audio/x-wav',
            'application/octet-stream',
        ];
    }

    public static function officeFiles()
    {
        return [
            //microsoft office
            'application/excel',
            'application/vnd.ms-excel',
            'application/x-excel',
            'application/x-msexcel',
            'application/msword',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            //open office
            'application/vnd.oasis.opendocument.spreadsheet',
            'application/vnd.oasis.opendocument.text',
        ];
    }

    public static function image()
    {
        return [
            'image/jpeg',
            'image/png',
            'image/gif',
            'image/x-ms-bmp'
        ];
    }

    public static function video()
    {
        return [
            'application/x-troff-msvideo',
            'video/avi',
            'video/msvideo',
            'video/x-msvideo',
            'video/mpeg',
            'video/quicktime',
            'video/mp4',
            'video/x-flv',
            'video/quicktime',
            'video/x-msvideo',
            'video/x-ms-wmv',
        ];
    }

    public static function pdf()
    {
        return [
            'application/pdf'
        ];
    }

    public static function all()
    {
        return array_merge(self::image(), self::audio(), self::video(), self::officeFiles(), self::pdf());
    }
}
