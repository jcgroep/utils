<?php namespace Jcgroep\Utils\ValueObjects\Files;

use App\Extension\Images\ImageExtension;

class ImageFile extends File
{

    public function getSubdir()
    {
        return 'images/';
    }

    public function resize()
    {
        ImageExtension::resizeImages($this->dirname(), $this->name(),
            [36 => 'small', '167' => 'medium', '480' => 'large']);
    }

    public function getType()
    {
        return 'image';
    }

    public function asHtml()
    {
        return '<p><a href="' . $this->getPublicPath() . '" data-lightbox="image-' . $this->name() . '" data-title="' . $this->name() . '"">
                        <img style="max-width: 100%" src="' . $this->getPublicPath() . '"/>
                    </a></p>';
    }

    public function getSmall()
    {
        return $this->getSize('small');
    }

    public function getLarge()
    {
        return $this->getSize('large');
    }

    public function getMedium()
    {
        return $this->getSize('medium');
    }

    public function getSize($size)
    {
        $expectedImage = new static($this->getDirectory() . '/' . $size . '/' . $this->name());
        if ($expectedImage->exists()) {
            return $expectedImage;
        }
        $this->resize();
        return $expectedImage;
    }

    public function getThumbnail()
    {
        return $this->getSmall()->getPublicPath();
    }

    public function put($contents, $lock = false)
    {
        $return = parent::put($contents, $lock);
        $this->resize();
        return $return;
    }

    public function delete()
    {
        $this->filesystem->delete($this->getSmall()->getAbsolutePath());
        $this->filesystem->delete($this->getMedium()->getAbsolutePath());
        $this->filesystem->delete($this->getLarge()->getAbsolutePath());
        return $this->filesystem->delete($this->getAbsolutePath());
    }

    public function getWidth()
    {
        return imagesx(imagecreatefromstring($this->filesystem->get($this->getAbsolutePath())));
    }

    public function getHeight()
    {
        return imagesy(imagecreatefromstring($this->filesystem->get($this->getAbsolutePath())));
    }
}
