<?php namespace Jcgroep\Utils\ValueObjects\Files;

class OfficeFile extends DownloadableFile
{
    public function getThumbnail()
    {
        if (in_array($this->mimeType(), [
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.oasis.opendocument.text',
            'application/msword',
        ])) {
            return '/images/word.png';
        }
        return '/images/excel.png';
    }

    public function getType()
    {
        return 'document';
    }
}
