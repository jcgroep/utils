<?php namespace Jcgroep\Utils\ValueObjects\Files;

class PdfFile extends DownloadableFile
{
    public function getThumbnail()
    {
        return '/images/pdf.png';
    }

    public function getType()
    {
        return 'pdf';
    }
}
