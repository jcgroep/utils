<?php

namespace Jcgroep\Utils\ValueObjects\Files;

use App\Utils\ValueObjects\Files\File as FileObject;
use Illuminate\Http\UploadedFile;
use File;

class TmpDirectory
{
    protected $uploadedFile;
    protected $directory;
    protected $tmpPath;
    protected $filename;

    public function __construct()
    {
        $this->tmpPath = storage_path('app/public/tmp/');
    }

    public static function make() {
        return new static;
    }

    public function withFile(UploadedFile $uploadedFile) {
        $this->uploadedFile = $uploadedFile;
        return $this;
    }

    public function withFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    public function inDirectory($directory) {
        $this->directory = $directory;
        return $this;
    }

    public function upload()
    {
        $file = FileObject::make($this->uploadedFile);
        if($file) {
            if($this->directory) {
                $file->move('tmp/'.$this->directory.'/'.$this->uploadedFile->getClientOriginalName());
            } else {
                $file->move('tmp/'.$this->uploadedFile->getClientOriginalName());
            }
        }
    }

    public function remove()
    {
        if($this->filename) {
            if($this->directory) {
                $this->tmpPath = $this->tmpPath . $this->directory . '/';
            }

            if(file_exists($this->tmpPath.$this->filename)) {
                unlink($this->tmpPath . $this->filename);
                if (count(File::glob($this->tmpPath . '*')) == 0) {
                    File::deleteDirectory($this->tmpPath);
                }
            }
        }
        return $this->filename;
    }

    public function getFiles()
    {
        if($this->directory) {
            $this->tmpPath = $this->tmpPath . $this->directory;
        }

        if(file_exists($this->tmpPath)) {
            $files = collect(File::allFiles($this->tmpPath));
            return $files->map(function($file) {
                return FileObject::make($file);
            });
        }

        return collect();
    }

    public function removeFolder()
    {
        if($this->directory) {
            if (count(File::glob($this->tmpPath . '/*')) == 0) {
                File::deleteDirectory($this->tmpPath);
            }
        }
    }
    public function removeAbandonedFolders()
    {
        if(file_exists($this->tmpPath)){
            $folders = collect(File::directories($this->tmpPath));

            $folders->filter(function($folder) {
                return (time()-filectime($folder)) > 60*60*24;
            })->each(function($folder) {
                File::deleteDirectory($folder);
            });
        }
    }
}