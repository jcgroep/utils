<?php namespace Jcgroep\Utils\ValueObjects\Files;

class AudioFile extends File
{
    public function getSubdir()
    {
        return 'audio/';
    }

    public function getType()
    {
        return 'audio';
    }

    public function asHtml()
    {
        return '<div><audio id="audioPlayer" class="pull-left" controls>
                    <source src="' . url($this->getPublicPath()) . '">
                    Your browser does not support the audio element.
                </audio>
                <div style="' . $this->getExtensionStyle() . '">
                <i style="margin-right: 15px;" onClick="rewindAudio(\'audioPlayer\')" class="fa fa-backward" aria-hidden="true"></i>
                <i onClick="skipAudio(\'audioPlayer\')" class="fa fa-forward" aria-hidden="true"></i>
                </div>
                </div>
                ';
    }

    public function getThumbnail()
    {
        return '/images/sound.png';
    }

    private function getExtensionStyle()
    {
        return 'padding-top: 7px;';
    }
}
