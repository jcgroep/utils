<?php namespace Jcgroep\Utils\ValueObjects\Files;

class DownloadableFile extends File
{
    public function asHtml()
    {
        return view('utils.file.downloadLink', ['file' => $this])->render();
    }
}
