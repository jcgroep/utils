<?php

namespace Jcgroep\Utils\ValueObjects\Files;

use Attachment;
use Illuminate\Http\UploadedFile;
use Message;

class DropZone
{
    protected $directory;
    protected $uploadedFile;
    protected $filename;

    public static function make()
    {
        return new static;
    }

    public function withDirectoryId($directory)
    {
        $this->directory = $directory;
        return $this;
    }

    public function withFile(UploadedFile $uploadedFile)
    {
        $this->uploadedFile = $uploadedFile;
        return $this;
    }

    public function withFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    public function upload()
    {
        TmpDirectory::make()
            ->inDirectory($this->directory)
            ->withFile($this->uploadedFile)
            ->upload();

        return $this->uploadedFile;
    }

    public function remove()
    {
        TmpDirectory::make()
            ->inDirectory($this->directory)
            ->withFilename($this->filename)
            ->remove();

        return $this->uploadedFile;
    }

    public function getFiles()
    {
        return TmpDirectory::make()
            ->inDirectory($this->directory)
            ->getFiles();
    }

    public function getFilesJson()
    {
        return $this->getFiles()->map(function($file) {
            return ['filename' => $file->name(), 'filesize' => $file->size(), 'url' => $file->getPublicPath()];
        });
    }

    public function moveToFinalDestination($finalDestination)
    {
        $files = TmpDirectory::make()
            ->inDirectory($this->directory)
            ->getFiles()
            ->map(function (File $file) use ($finalDestination) {
                return $file->move($finalDestination);
            });

        TmpDirectory::make()
            ->inDirectory($this->directory)
            ->removeFolder();

        TmpDirectory::make()
            ->removeAbandonedFolders();

        return $files;
    }


}