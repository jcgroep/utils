<?php namespace Jcgroep\Utils\ValueObjects\Files;

class VideoFile extends File
{

    public function getSubdir()
    {
        return 'videos/';
    }

    public function getType()
    {
        return 'video';
    }
}
