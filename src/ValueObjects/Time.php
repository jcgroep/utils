<?php namespace Jcgroep\Utils\ValueObjects;

class Time extends ValueObject
{
    public static function isValid($value)
    {
        return preg_match('/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $value);
    }

    public function getHours()
    {
        return explode(':', $this->value)[0];
    }

    public function getMinutes()
    {
        return explode(':', $this->value)[1];
    }

    public function toCarbon()
    {
        return \Carbon::createFromTime($this->getHours(), $this->getMinutes());
    }

    public function diffInMinutes(Time $compare)
    {
        return $compare->toCarbon()->diffInMinutes($this->toCarbon());
    }

    public static function getErrorMessage($value)
    {
        return \trans('errors.invalidTime', ['value' => $value]);
    }
}
