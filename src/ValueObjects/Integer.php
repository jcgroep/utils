<?php namespace Jcgroep\Utils\ValueObjects;

class Integer extends ValueObject
{
    public static function isValid($value)
    {
        return is_numeric($value);
    }

    public function between($min, $max)
    {
        return $this->value >= $min && $this->value <= $max;
    }
}
