<?php namespace Jcgroep\Utils\ValueObjects;

class ValueObject
{
    public $value;

    public static function make($value)
    {
        return new static($value);
    }

    public function __construct($value)
    {
        if (!self::isValid($value)) {
            throw new \InvalidArgumentException(self::getErrorMessage($value));
        }
        $this->value = $value;
    }

    public static function isValid($value)
    {
        return true;
    }

    public function __toString()
    {
        return $this->value;
    }

    public static function getErrorMessage($value)
    {
        return 'Value "' . $value . '" is not a valid value for a ' . get_called_class();
    }
}
