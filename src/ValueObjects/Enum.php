<?php namespace Jcgroep\Utils\ValueObjects;

use BadMethodCallException;
use \Lang;
use ReflectionClass;

class Enum extends ValueObject
{

    public static function all()
    {
        $reflection = new ReflectionClass(get_called_class());
        return array_combine($reflection->getConstants(), $reflection->getConstants());
    }

    public function getTranslation()
    {
        if (array_key_exists($this->value, static::all())) {
            return static::all()[$this->value];
        }
        return ucfirst(trans('global.unknown'));
    }

    public static function isValid($value)
    {
        return array_key_exists($value, static::all());
    }

    public function __toString()
    {
        return $this->value;
    }

    public static function __callStatic($name, $arguments)
    {
        if(in_array($name, self::all())){
            return new static($name);
        }
        throw new BadMethodCallException('Call to undefined method ' . get_called_class() . '::' . $name . '()');
    }
}
