<?php namespace Jcgroep\Utils\ValueObjects;

use Jcgroep\Utils\Collections\BaseCollection;


/**
 * Class Str Cannot use String because 'String' is a special class name
 */
class StringObject extends ValueObject
{
    public function stripTags($allowTags = null, $replaceBy = ' '): StringObject
    {
        $spaceString = str_replace('<', $replaceBy . '<', $this->value);
        return new static(str_replace('  ', ' ', strip_tags($spaceString, $allowTags)));
    }

    public function substr($start, $stop, $ellipsis = false): StringObject
    {
        $substr = substr($this->value, $start, $stop);
        if (strlen($this->value) > $stop && $ellipsis) {
            $substr .= '...';
        }
        return new static($substr);
    }

    public function trim($charList = ' \t\n\r\0\x0B')
    {
        $this->value = trim($this->value, $charList);
        return $this;
    }

    public function explode($delimiter): BaseCollection
    {
        return BaseCollection::make(explode($delimiter, $this->value));
    }

    public function isNumeric()
    {
        return is_numeric($this->value);
    }

    public function isEmpty()
    {
        return empty($this->stripTags()->__toString());
    }

    public function toInt()
    {
        return (int) $this->value;
    }

    public function replace($search, $replace)
    {
        return new static(str_replace($search, $replace, $this->value));
    }

    public function removeSqlSpecialCharacters()
    {
        $this->value = preg_replace('([\'"%_+\-\(\)\[\]])', '', $this->value);
        return $this;
    }

    public static function sanitize( $string) {
        $string = str_replace("\n", "", $string);
        $string = str_replace("\r", "", $string);
        $string = str_replace("\t", "", $string);
        return $string;
    }
}
