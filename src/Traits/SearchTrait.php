<?php namespace Jcgroep\Utils\Traits;

use Illuminate\Database\Eloquent\Builder;
use Jcgroep\Utils\ValueObjects\StringObject;

trait SearchTrait
{
    /**
     * @var StringObject
     */
    protected $search;

    public function search($search)
    {
        $this->search = StringObject::make($search)
            ->trim();
        return $this;
    }

    protected function searchInQuery(Builder $query)
    {
        if (isset($this->search) && !$this->search->isEmpty()) {
            $search = $this->search
                ->removeSqlSpecialCharacters()
                ->explode(' ')
                ->map(function ($word) {
                    return '\'%' . $word . '%\'';
                })
                ->join(' AND `title` LIKE ');
            $query->whereRaw('`title` LIKE ' . $search);
        }
    }
}

